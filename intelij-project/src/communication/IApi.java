package communication;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by sttormax on 15.03.2017.
 */
public interface IApi extends Remote {

    static final String LOOKUP_NAME  = "people-api";

    String getPeople() throws RemoteException;
    String getPerson(String email) throws RemoteException;
    String editPerson(String email, String field, String value) throws RemoteException;

}
