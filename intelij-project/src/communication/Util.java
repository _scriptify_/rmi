package communication;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by sttormax on 27.03.2017.
 */
public class Util {

    private static final String[] allowedFields = {
            "firstName",
            "lastName",
            "email",
            "gender",
            "description",
            "city",
            "addictedTo",
            "country",
            "address"
    };

    public static void appendToJSON(StringBuilder json, String name, String value, boolean isLast) {
        json.append("\"");
        json.append(name);
        json.append("\":");
        json.append("\"");
        json.append(value);
        json.append("\"");
        if(!isLast)
            json.append(",");
    }

    public static void appendToJSON(StringBuilder json, String name, String value) {
        Util.appendToJSON(json, name, value, false);
    }

    public static void resultToJSON(ResultSet res, StringBuilder json) throws SQLException {
        json.append("{");

        for(int i = 0; i < Util.allowedFields.length; i++) {
            boolean isLast = false;
            if(i == ( Util.allowedFields.length - 1 )) {
                isLast = true;
            }
            Util.appendToJSON(json, Util.allowedFields[i], res.getString(Util.allowedFields[i]), isLast);
        }


        json.append("},");
    }

    public static String[] getFields() {
        return Util.allowedFields;
    }


}
