package communication;

import java.rmi.RemoteException;
import java.sql.*;

/**
 * Created by sttormax on 15.03.2017.
 */
public class Api implements IApi {

    private String dbUsername;
    private String dbPass;
    private String dbLocation;
    private Connection db;

    public Api(String dbUsername, String dbPass, String dbLocation) {
        this.dbUsername = dbUsername;
        this.dbPass = dbPass;
        this.dbLocation = dbLocation;
        this.connectToDb();
    }

    private void connectToDb() {

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        String url = "jdbc:mysql://" + this.dbLocation;
        System.out.println(url);
        try {
            this.db = DriverManager.getConnection(url, this.dbUsername, this.dbPass);
        } catch (SQLException e) {
            System.out.println("Error while connecting to database. ");
            e.printStackTrace();
        }
    }

    @Override
    public String getPeople() throws RemoteException {
        String query = "SELECT email, firstName, lastName FROM Person";
        Statement s = null;
        StringBuilder json = new StringBuilder();
        json.append("[");
        try {
            s = this.db.createStatement();
            ResultSet res = s.executeQuery(query);
            while(res.next()) {
                json.append("{");
                Util.appendToJSON(json, "firstName", res.getString("firstName"));
                Util.appendToJSON(json, "lastName", res.getString("lastName"));
                Util.appendToJSON(json, "email", res.getString("email"), true);
                json.append("},");
            }

            json.deleteCharAt( json.length() - 1 );
        } catch (SQLException e) {
            System.out.println("SQL error: " + e.toString());
        }

        json.append("]");

        System.out.println(json.toString());

        return json.toString();
    }

    @Override
    public String getPerson(String emailU) throws RemoteException {
        StringBuilder json = new StringBuilder();
        try {

            String query = "SELECT * FROM Person WHERE email = ?;";
            PreparedStatement ps = this.db.prepareStatement(query);
            ps.setString(1, emailU);
            json.append("[");

            ResultSet res = ps.executeQuery();
            while(res.next()) {
                Util.resultToJSON(res, json);
            }

            json.deleteCharAt( json.length() - 1 );
        } catch (SQLException e) {
            System.out.println("SQL error: " + e.toString());
        }

        json.append("]");

        return json.toString();
    }

    @Override
    public String editPerson(String email, String field, String value) throws RemoteException {
        StringBuilder json = new StringBuilder();
        try {
            boolean isIn = false;
            for(String s : Util.getFields()) {
                if(s.equals(field)) {
                    isIn = true;
                    break;
                }
            }

            if(!isIn) {
                throw new RemoteException("Invalid field '" + field + "'!");
            }

            String query = "UPDATE Person SET " + field + " = ? WHERE email = ?;";
            PreparedStatement ps = this.db.prepareStatement(query);
            ps.setString(1, value);
            ps.setString(2, email);
            json.append("[");

            ps.executeUpdate();

            query = "SELECT * FROM Person WHERE email = ?;";
            ps = this.db.prepareStatement(query);
            ps.setString(1, email);
            ResultSet res = ps.executeQuery();
            while(res.next()) {
                Util.resultToJSON(res, json);
            }

            json.deleteCharAt( json.length() - 1 );
        } catch (SQLException e) {
            System.out.println("SQL error: " + e.toString());
        }

        json.append("]");

        return json.toString();
    }
}
