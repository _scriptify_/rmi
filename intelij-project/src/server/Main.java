package server;

/**
 * Created by sttormax on 14.03.2017.
 */
public class Main {

    public static void main(String args[]) {
        String dbServer = "linuxserver";
        String dbUser = "sttormax";
        String dbPass = "mypass";
        String dbName = "DB4_sttormax";

        switch(args.length) {
            case 1: {
                if(args[0].equals("help")) {
                    System.out.println("Usage: <db-user> <db-pass> <db-tablename> <db-server>");
                    return;
                }
                dbUser = args[0];
                break;
            }

            case 2: {
                dbUser = args[0];
                dbPass = args[1];
                break;
            }

            case 3: {
                dbUser = args[0];
                dbPass = args[1];
                dbName = args[2];
                break;
            }

            case 4: {
                dbUser = args[0];
                dbPass = args[1];
                dbName = args[2];
                dbServer = args[4];
                break;
            }
        }

        if(args.length == 1 && args[0].equals("help")) {
            System.out.println("Usage: <db-user> <db-pass> <db-tablename> <db-server>");
            return;
        }

        Server s = new Server(dbUser, dbPass, dbName, dbServer);
    }

}
