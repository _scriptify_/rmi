package server;

import communication.Api;
import communication.IApi;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by sttormax on 14.03.2017.
 */
public class Server {

    private Api api;

    public Server(String dbUser, String dbPass, String dbName, String dbServer) {
        this(8080, dbUser, dbPass, dbName, dbServer);
    }

    public Server(int registryPort, String dbUser, String dbPass, String dbName, String dbServer) {

        /*if(System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }*/

        this.api = new Api(dbUser, dbPass, dbServer + "/" + dbName);

        try {

            IApi stub = (IApi) UnicastRemoteObject.exportObject(this.api, 0);
            Registry registry = LocateRegistry.getRegistry(registryPort);


            String[] services = registry.list();
            boolean isBound = false;
            for(String service : services) {
                if(service.equals(IApi.LOOKUP_NAME)) {
                    isBound = true;
                }
            }

            if(!isBound)
                registry.bind(IApi.LOOKUP_NAME, stub);

            System.out.println("Server ready.");
        } catch (RemoteException e) {
            System.out.println("Error whilst server execution: " + e.toString());
        } catch (AlreadyBoundException e) {
            System.out.println("The stub with this name is already bound!");
        }
    }

}
