package client;

import communication.Util;

/**
 * Created by sttormax on 14.03.2017.
 */
public class Main {

    public static void main(String args[]) {

        if(args.length == 1 && args[0].equals("help")) {
            System.out.println("Welcome to the drug agency communication client. It offers 3 functionalities:");
            System.out.println("[1] Get all people:\n\t <host> <port> get-people");
            System.out.println("[2] Get a specific persons information:\n\t <host> <port> get-person <email>");
            System.out.println("[3] Set a specific persons field:\n\t <host> <port> edit-person <email> <field> <value>");
            System.out.println("\tValid fields are:");
            for(String field : Util.getFields()) {
                System.out.println("\t\t" + field);
            }
        }

        if(args.length < 3) {
            System.out.println("Missing parameters! Usage: <host> <port> <method> [params].\nType 'help' for help.");
            return;
        }

        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String method = args[2];
        String params = "";

        if(method.equals("get-person")) {
            if(args.length < 4) {
                System.out.println("Missing parameters! Usage: <host> <port> get-person <email>");
                return;
            }
            params = args[3];
        } else if(method.equals("edit-person")) {
            if(args.length < 6) {
                System.out.println("Missing parameters! Usage: <host> <port> get-person <email> <field> <value>");
                return;
            }
            params = args[3] + " " + args[4] + " " + args[5];
        }

        Client c = new Client(host, port, method, params);
    }

}
