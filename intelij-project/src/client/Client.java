package client;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;
import communication.*;
import jdk.nashorn.internal.parser.JSONParser;

import java.io.StringReader;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;

/**
 * Created by sttormax on 14.03.2017.
 */
public class Client {

    private IApi api;
    private String[] params;

    public Client(String host, int port, String cmd, String params) {

        System.out.println("Trying to connect to " + host + ":" + port);
        this.params = params.split(" ");

        try {
            Registry registry = LocateRegistry.getRegistry(port);
            IApi stub = (IApi) registry.lookup(IApi.LOOKUP_NAME);
            this.api = stub;
            String ret = this.execute(cmd);
            //System.out.println("From RMI: " + ret);
            JsonArray arr = Json.parse(ret).asArray();
            for (JsonValue value : arr) {
                JsonObject obj = value.asObject();
                if(cmd.equals("get-people")) {
                    String firstName = obj.get("firstName").asString();
                    String lastName = obj.get("lastName").asString();
                    String email = obj.get("email").asString();
                    System.out.println("\tfirstName: " + firstName);
                    System.out.println("\tlastName: " + lastName);
                    System.out.println("\temail: " + email);
                } else {
                    for(String field : Util.getFields()) {
                        String actualVal = obj.get(field).asString();
                        System.out.println("\t" + field + ": " + actualVal);
                    }
                }
                System.out.println("\n");
            }
        } catch (RemoteException e) {
            System.out.println("Error whilst client execution: " + e.toString());
        } catch (NotBoundException e) {
            System.out.println("Didn't find that stub!");
        }
    }

    private String execute(String cmd) throws RemoteException {

        String ret = "";
        switch(cmd) {
            case "get-people":
                ret = this.api.getPeople();
                break;
            case "get-person":
                String email = this.params[0];
                ret = this.api.getPerson(email);
                break;
            case "edit-person":
                String mail = this.params[0];
                String field = this.params[1];
                String value = this.params[2];
                ret = this.api.editPerson(mail, field, value);
                break;
        }

        return ret;
    }

}
