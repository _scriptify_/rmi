package registry;

import javax.swing.*;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

/**
 * Created by sttormax on 20.03.2017.
 */
public class NameService {

    public static void main(String args[]) {
        new NameService();
        JOptionPane.showConfirmDialog(null, "RMI-Registry started. Click OK to exit.");
    }

    public NameService() {
        this(8080);
    }

    public NameService(int port) {
        try {
            LocateRegistry.createRegistry(port);
        } catch (RemoteException e) {
            System.out.println("Could not create registry.");
        }
    }

}
