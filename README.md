# RMI - �bung mit SQL Anbindung

Verschiedenste Mitarbeiter der Drogenbeh�rde k�nnen �ber den RMI Client auf die Datenbank der Beh�rde zugreifen.
Dabei k�nnen entweder alle Personen angezeigt werden oder nur die bestimmten Informationen einer Person abgefragt werden.
Au�erdem kann ein Mitarbeiter eine Person bearbeiten.

## Schritt 1: Registry starten
java -jar Registry.jar

## Schritt 2: Server starten
java -jar Server.jar
Diese ausf�hrbare jar-Datei akzeptiert folgende Parameter zur Datenbankkonfiguration:  <db-user> <db-pass> <db-tablename> <db-server>.

## Schritt 3: Server starten
java -jar Client.jar

Der Client kann folgenderma�en verwendet werden:

[1] Alle Personen anzeigen:
         <host> <port> get-people
		 z.B. java -jar Client.jar 127.0.0.1 8080 get-people
[2] Die Informationen einer bestimmten Person abrufen:
         <host> <port> get-person <email>
		 z.B. java -jar Client.jar 127.0.0.1 8080 get-person foo@bar.com
[3] Ein Datenfeld einer bestimmten Person setzen:
         <host> <port> edit-person <email> <field> <value>
		 z.B. java -jar Client.jar 127.0.0.1 8080 edit-person foo@bar.com addictedTo programming
        G�ltige Felder sind:
                firstName
                lastName
                email
                gender
                description
                city
                addictedTo
                country
                address

